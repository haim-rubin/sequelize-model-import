
const con = require('./connection')
const sequelize = require('sequelize')
const MAP_NULLABLE_TO_BOOLEAN = { 
	NO: false,
	YES: true
}



mapIsNullable = {
	NO: false,
	YES: true
}

const MAP_TO_SEQUELIZE_TYPE = {
	int: 'INTEGER'	
}

const getSequelizeType = (column) => {
	switch(column.DATA_TYPE){
		case 'varchar':
			return Sequelize.STRING(column.CHARACTER_MAXIMUM_LENGTH)
		default:
			return Sequelize[MAP_TO_SEQUELIZE_TYPE[column.DATA_TYPE]]
	}

	
}

const runQuery = (query) => {
	return con.query(query, { type: sequelize.QueryTypes.SELECT})
}

const getSchemaTables = (database) => {
	const query = `SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG='${database}'` //(for MySql, use: TABLE_SCHEMA='dbName' )
	return runQuery(query)
}

const getTableDefinition = (table) =>{	
	const query = `Select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='${table}'`
	return runQuery(query)
			.then(columns => 
				getTableIsPrimaryKey(table)
					.then(primaryKeys => 
						({ columns, primaryKeys})
					)
			)

}
const getAllTablesDefinition = (database) =>{
 	return new Promise((resolve, reject) => {
		getSchemaTables(database)
			.then(tables => tables.map(table => table.TABLE_NAME))			
		 	
		 	.then(tables => 
		 			Promise.all(
		 				tables.map(table => 
		 					getTableDefinition(table)
		 				)
		 			)
		 			.then(resolve)
		 		)

 	})
}

const getTableIsPrimaryKey = (table) => {
	const query = `SELECT Col.Column_Name from 
					    INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab, 
					    INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col 
					WHERE 
					    Col.Constraint_Name = Tab.Constraint_Name
					    AND Col.Table_Name = Tab.Table_Name
					    AND Constraint_Type = 'PRIMARY KEY'
					    AND Col.Table_Name = '${table}'`

	return runQuery(query)
}

const getTableAsSequilizeDefinition = ({ columns, primaryKeys }) => {
	const seqDef = 
		columns
			.map(column => {
				const { COLUMN_NAME, DATA_TYPE, IS_NULLABLE } = column
				const primaryKey =  primaryKeys.length === 1 && COLUMN_NAME === primaryKeys[0]['Column_Name'] && { primaryKey: true } || {}
				return {
					[COLUMN_NAME]: 
						Object.assign({}, {
								field: COLUMN_NAME,
								type: DATA_TYPE,
								allowNull: mapIsNullable[IS_NULLABLE],	 
							}, 
							primaryKey
						)
				}
			})
			.reduce((col1, col2) => Object.assign({}, col1, col2), {})

	return { 
		key: columns[0].TABLE_NAME,
		value: seqDef
	}
	
}


const getAllTablesDefinitionSequelize = (database) => {
	return getAllTablesDefinition(database)
			.then(tables => tables.map(getTableAsSequilizeDefinition))			

}

module.exports = { getSchemaTables, getTableDefinition, getAllTablesDefinition, getTableIsPrimaryKey, getAllTablesDefinitionSequelize}
	
	