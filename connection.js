const Sequelize = require('sequelize')
const settings = require('./settings')

class DBConnection{
	constructor(settings){		
		const sequelize = new Sequelize(settings.database, 
										settings.user, 
										settings.password, 
										settings)
		return sequelize
	}
}

module.exports = new DBConnection(settings)